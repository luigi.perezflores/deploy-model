# Deploy Model

Project shows example of Online Deployment for a ML model using Pickle object, Docker and FastAPI.

## 01 Description

1. Jupyter Notebooks shows the creationg of the ML model and save it as a pickle object.
2. Dockerfile to create the isolated environment (use of requirements.txt).
3. FastAPI to deploy the model into production.

## 02 Training Data

Local or s3 Bucket:

https://us-east-1.console.aws.amazon.com/s3/buckets/moneylion-project?region=us-east-1&bucketType=general&tab=objects
 

## 03 How to run?

1. Build the image using the Dockerfile.
2. Run the container and open it in the browser.

Example:

![captura_app](./images/captura_app.png)