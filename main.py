import joblib
import pandas as pd
import uvicorn

from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from pydantic import BaseModel

# Initialize FastAPI
app = FastAPI()
model = joblib.load('model/model_lgbm.pkl')

# Servir archivos estáticos
#app.mount("/static", StaticFiles(directory="static"), name="static")

# Configurar plantillas
templates = Jinja2Templates(directory="html")

# Model Input
class PredictRequest(BaseModel):
    ninetydaysago : float = 4
    threesixtyfivedaysago : float = 4
    workphonepreviouslylistedascellphone : bool = False
    clearfraudscore : float = 556
    apr : float = 645
    loanAmount : float = 400
    nameaddressmatch : str = "invalid"
    phonematchtype : str = "M"
    phonematchresult : str = "unavailable"
    overallmatchresult : str = "other"
    payFrequency : str = "B"
    state : str = "IA"
    leadType : str = "lead"

# Model Output
class PredictionResponse(BaseModel):
    predicted_class: int
    predicted_probability: float

# APP
@app.get("/", response_class=HTMLResponse)
async def get_form(request: Request):
    return templates.TemplateResponse("risk_model_app.html", {"request": request})


@app.post('/predict')
async def predict(
    ninetydaysago: float = Form(...),
    threesixtyfivedaysago: float = Form(...),
    workphonepreviouslylistedascellphone: bool = Form(...),
    clearfraudscore: float = Form(...),
    apr: float = Form(...),
    loanAmount: float = Form(...),
    nameaddressmatch: str = Form(...),
    phonematchtype: str = Form(...),
    phonematchresult: str = Form(...),
    overallmatchresult: str = Form(...),
    payFrequency: str = Form(...),
    state: str = Form(...),
    leadType: str = Form(...)
):
    data = PredictRequest(
        ninetydaysago=ninetydaysago,
        threesixtyfivedaysago=threesixtyfivedaysago,
        workphonepreviouslylistedascellphone=workphonepreviouslylistedascellphone,
        clearfraudscore=clearfraudscore,
        apr=apr,
        loanAmount=loanAmount,
        nameaddressmatch=nameaddressmatch,
        phonematchtype=phonematchtype,
        phonematchresult=phonematchresult,
        overallmatchresult=overallmatchresult,
        payFrequency=payFrequency,
        state=state,
        leadType=leadType
    )
    data = pd.DataFrame([data.model_dump()])

    CAT_VARS = [
        'nameaddressmatch',
        'phonematchtype',
        'phonematchresult',
        'overallmatchresult',
        'payFrequency',
        'state',
        'leadType'
    ]
    
    for feature in CAT_VARS:
        data[feature] = pd.Series(data[feature], dtype="category")
        
    score = model.predict_proba(data)[:,1][0]
    y_pred = model.predict(data)[0]
    
    return PredictionResponse(
        predicted_class=y_pred,
        predicted_probability=score)
        
if __name__=='__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)

## RUN API ##
# uvicorn main:app --reload
# http://localhost:8000